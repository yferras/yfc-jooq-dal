drop table if exists public.person;

create table if not exists public.person (
	id serial not null,
	"name" varchar(64) not null,
	last_name varchar(64) not null,
	constraint person_pk primary key(id),
	constraint person_uq unique("name", last_name)
);

INSERT INTO public.person
	(id, "name", last_name)
values
	(1, 'A', 'B'),
	(2, 'B', 'C'),
	(3, 'X', 'Y'),
	(4, 'A', 'X'),
	(5, 'A', 'Y')
;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
drop table if exists public.company;

create table if not exists public.company (
	id serial not null,
	"name" varchar(64) not null,
	geo_lat int not null,
	geo_lon int not null,
	constraint company primary key(id),
	constraint company_uq unique("name", geo_lat, geo_lon)
);

