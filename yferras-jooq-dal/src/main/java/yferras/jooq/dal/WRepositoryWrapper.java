package yferras.jooq.dal;

/**
 * @param <E>  Any type
 * @param <K>  Any type from the key of the record.
 * @param <R> Any class derived from {@link WRepository}
 */
public interface WRepositoryWrapper<E, K, R extends WRepository<E, K, ?, ?, ?, ?, ?, ?>> {

    /**
     * Gets the wrapped repository.
     *
     * @return an instance of any type derived from: {@link WRepository}
     */
    R getWrappedRepository();

}
