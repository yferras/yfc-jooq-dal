package yferras.jooq.dal;

import java.util.Collection;

/**
 * Write repository.
 *
 * @param <M0>  Any type for the result of {@link #insert(Object)}.
 * @param <M1> Any type for the result of {@link #insert(Object[])} or {@link #insert(Collection)}.
 * @param <M2>  Any type for the result of {@link #update(Object)}.
 * @param <M3> Any type for the result of {@link #update(Object[])} or {@link #update(Collection)}.
 * @param <M4>  Any type for the result of {@link #delete(Object)}.
 * @param <M5> Any type for the result of {@link #delete(Object[])} or {@link #delete(Collection)}.
 */
public interface WRepository<E, K, M0, M1, M2, M3, M4, M5> {

    /**
     * Inserts one entity.
     *
     * @param entity source.
     * @return any information object.
     */
    M0 insert(E entity);

    /**
     * Inserts one or more entities, comma separated.
     *
     * @param entities an array of entities.
     * @return any information object.
     */
    M1 insert(E... entities);

    /**
     * Inserts one or more entities.
     *
     * @param entities source.
     * @return any information object.
     */
    M1 insert(Collection<E> entities);

    /**
     * Updates one entity.
     *
     * @param entity source.
     * @return any information object.
     */
    M2 update(E entity);

    /**
     * Updates one or more entities.
     *
     * @param entities an array of entities.
     * @return any information object.
     */
    M3 update(E... entities);

    /**
     * Updates one or more entities.
     *
     * @param entities source.
     * @return any information object.
     */
    M3 update(Collection<E> entities);

    /**
     * Deletes the entity by its id.
     *
     * @param id the identifier of the entity.
     * @return any information object.
     */
    M4 delete(K id);

    /**
     * Deletes the entities by its id.
     *
     * @param ids the identifier array.
     * @return any information object.
     */
    M5 delete(K... ids);

    /**
     * Deletes the entities by its id.
     *
     * @param ids the identifier collection.
     * @return any information object.
     */
    M5 delete(Collection<K> ids);
}
