package yferras.jooq.dal.common;

public enum Action {
    INSERT,
    BATCH_INSERT,
    UPDATE,
    BATCH_UPDATE,
    DELETE,
    BATCH_DELETE,
    SELECT,
    SELECT_BY_ID,
    SELECT_BY_EXAMPLE
}
