package yferras.jooq.dal.simple;

import yferras.jooq.dal.RWRepository;
import yferras.util.pagination.Page;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @param <E> Any type
 * @param <K> Any type from the key of the record.
 */
public interface SimpleRWRepository<E extends Serializable, K> extends
        SimpleRRepository<E, K>,
        SimpleWRepository<E, K>,
        RWRepository<E, K, Optional<E>, List<E>, Boolean, Page<E>, Integer, Integer, Integer, Integer, Integer, Integer> {

}
