package yferras.jooq.dal.simple;

import lombok.extern.slf4j.Slf4j;
import org.jooq.*;
import yferras.util.pagination.DefaultPage;
import yferras.util.pagination.DefaultPageRequest;
import yferras.util.pagination.Page;
import yferras.util.pagination.PageRequest;

import java.text.MessageFormat;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;


@Slf4j
public abstract class AbstractSimpleRRepository<R extends TableRecord<?>, K> implements SimpleRRepository<R, K> {

    protected abstract DSLContext getDsl();

    @Override
    public Optional<R> get(K id) {
        R record = getDsl()
                .select(getTable().fields()) // select *
                .from(getTable())            // from TABLE_NAME
                .where(                      // where
                        getIdField().eq(id)  //     id = VALUE
                ).fetchOneInto(getTable());
        return Optional.ofNullable(record);
    }

    @Override
    public List<R> getByExample(R example) {
        Objects.requireNonNull(example);

        if (isEmpty(example)) {
            return Collections.emptyList();
        }

        return getAll(DefaultPageRequest.of(1, 10, Collections.singletonList(example))).getList();
    }

    @Override
    public Boolean exists(R example) {
        Objects.requireNonNull(example);

        if (isEmpty(example)) {
            return false;
        }

        Page<R> page = getAll(DefaultPageRequest.of(1, 1, Collections.singletonList(example)));
        return page.getResultCount() != 0;
    }

    protected final boolean isEmpty(R example) {
        return Arrays.stream(example.fields()).allMatch(field -> example.get(field) == null);
    }

    @Override
    public Page<R> getAll(PageRequest pageRequest) {
        List<Condition> conditions = getConditionFromFilter(pageRequest.getFilters());
        return getAll(pageRequest, conditions);
    }

    protected Page<R> getAll(PageRequest pageRequest, Condition... conditions) {
        return getAll(pageRequest, Arrays.asList(conditions));
    }

    protected Page<R> getAll(PageRequest pageRequest, List<Condition> conditions) {
        Long count = getDsl().selectCount().from(getTable()).where(conditions).fetchOneInto(Long.class);

        if (Objects.isNull(count) || count == 0L) {
            return DefaultPage.empty(pageRequest);
        }

        Result<R> result = getDsl()
                .select(getTable().fields())
                .from(getTable())
                .where(conditions).orderBy(getIdField())
                .limit(pageRequest.getSize())
                .offset((pageRequest.getPage() - 1) * pageRequest.getSize())
                .fetchInto(getTable());
        return DefaultPage.of(new ArrayList<>(result), pageRequest, count);
    }

    protected abstract Field<K> getIdField();

    protected abstract Table<R> getTable();

    private List<Condition> getConditionFromFilter(List<R> list) {
        if (list == null ||
                list.isEmpty() ||
                list.stream().allMatch(Objects::isNull)
        ) {
            return Collections.emptyList();
        }
        return buildConditionsFromRecords(list.stream().filter(Objects::nonNull).collect(Collectors.toList()));
    }

    /**
     * Builds a list of conditions from the input list. By default it does nothing more than return an empty list of
     * conditions. If you want to filter the results you must override its implementation.
     * <p>
     * The input list will be: not null, not empty and don't equalsOrContains null values.
     *
     * @param list input examples.
     * @return list of conditions.
     */
    private List<Condition> buildConditionsFromRecords(List<R> list) {
        if (list.isEmpty()) {
            return Collections.emptyList();
        }
        Field<?>[] fields = getTable().fields();
        Condition orCondition = null;
        for (R r : list) {
            Field<?> field = fields[0];
            Condition andCondition = getCondition(r, field, getOperatorByField(field));
            for (int j = 1; j < fields.length; j++) {
                field = fields[j];
                andCondition = getCondition(andCondition, r, field, getOperatorByField(field));
            }
            if (andCondition == null) {
                continue;
            }
            orCondition = orCondition == null ? andCondition : orCondition.or(andCondition);
        }

        return orCondition == null ? Collections.emptyList() : Collections.singletonList(orCondition);
    }

    protected <T> BiFunction<T, Field<T>, Condition> getOperatorByField(Field<?> field) {
        return eq();
    }

    protected static <T> BiFunction<T, Field<T>, Condition> equalsOrContains() {
        return (value, field) -> {
            if (String.class.equals(field.getType()) && value instanceof String) {
                return field.eq(value).or(field.contains(value));
            }
            throw new IllegalArgumentException(
                    MessageFormat.format("Operator equalsOrContains not applicable to field: {0} an value {1}",
                            field.getName(),
                            value
                    )
            );
        };
    }

    protected static <T> BiFunction<T, Field<T>, Condition> eq() {
        return (value, field) -> field.eq(value);
    }

    protected static <T> BiFunction<T, Field<T>, Condition> equalIgnoreCase() {
        return (value, field) -> {
            if (String.class.equals(field.getType()) && value instanceof String) {
                return field.equalIgnoreCase(String.valueOf(value));
            }
            throw new IllegalArgumentException(
                    MessageFormat.format("Operator equalIgnoreCase not applicable to field: {0} an value {1}",
                            field.getName(),
                            value
                    )
            );
        };
    }

    protected static <T> BiFunction<T, Field<T>, Condition> isNull() {
        return (value, field) -> field.isNull();
    }

    @SuppressWarnings("unchecked")
    protected static <T> BiFunction<T, Field<T>, Condition> between(Number minValue, Number maxValue) {
        return (value, field) -> {
            if (Number.class.isAssignableFrom(field.getType())) {
                Field<Number> numberField = (Field<Number>) field;
                return numberField.between(minValue, maxValue);
            }
            throw new IllegalArgumentException(
                    MessageFormat.format("Operator between not applicable to field: {0} and values min: {1} and max: {2}",
                            field.getName(),
                            minValue,
                            maxValue
                    )
            );
        };
    }

    protected static <T> Condition getCondition(
            Condition condition,
            Record record,
            Field<T> field,
            BiFunction<T, Field<T>, Condition> biFunction) {
        T value = record.get(field);
        if (Objects.nonNull(value)) {
            Condition condition1 = biFunction.apply(value, field);
            return Objects.isNull(condition) ? condition1 : condition.and(condition1);
        }
        return condition;
    }

    protected static <T> Condition getCondition(
            Record record,
            Field<T> field,
            BiFunction<T, Field<T>, Condition> biFunction) {
        return getCondition(null, record, field, biFunction);
    }

    private List<TableField<R, ?>> getTableUniqueFields() {
        return getTable()
                .getKeys()
                .stream()
                .filter(key -> !key.isPrimary())
                .findFirst()
                .map(Key::getFields)
                .orElse(Collections.emptyList());
    }

    /**
     * Gets the records by the first unique key.
     *
     * @param record record to find.
     * @return an instance of {@link Optional}.
     */
    protected Optional<Result<R>> getRecordsByUniqueKey(R record) {

        List<TableField<R, ?>> fields = getTableUniqueFields();
        if (isEmpty(record) || fields.isEmpty()) {
            return Optional.empty();
        }

        Condition condition = getCondition(record, fields.get(0), eq());
        for (int i = 1; i < fields.size(); i++) {
            condition = getCondition(condition, record, fields.get(i), eq());
        }
        Result<R> result = getDsl()
                .select()
                .from(getTable())
                .where(condition)
                .fetchInto(getTable());
        return result.isEmpty() ? Optional.empty() : Optional.of(result);
    }

    protected Map<String, Object> getValuesFromUnique(R record) {
        List<TableField<R, ?>> fields = getTableUniqueFields();
        if (fields.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String, Object> map = new LinkedHashMap<>();
        for (TableField<R, ?> rTableField : fields) {
            map.put(rTableField.getName(), rTableField.getValue(record));
        }

        return map;
    }

}
