package yferras.jooq.dal.simple;

import lombok.extern.slf4j.Slf4j;
import org.jooq.UpdatableRecord;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;


@Slf4j
public abstract class AbstractSimpleRWRepository<R extends UpdatableRecord<?>, K> extends AbstractSimpleRRepository<R, K>
        implements SimpleRWRepository<R, K> {

    /* * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                INSERT                                                          *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * To performs validations and some stuff before inserting the record in the database.
     *
     * @param record instance of any class derived from {@link UpdatableRecord}.
     * @return {@code true} if this record is absolutely valid for insertion; otherwise returns {@code false}.
     */
    protected boolean beforeInsert(R record) {
        return record != null;
    }

    /**
     * To do some stuff after record was inserted in the database.
     *
     * @param record instance of any class derived from {@link UpdatableRecord}.
     */
    protected void afterInsert(R record) {
        
    }

    /**
     * To performs validations and some stuff before inserting the records in the database.
     *
     * @param records a collection of instances of any class derived from {@link UpdatableRecord}.
     * @return a collection of instances of any class derived from {@link UpdatableRecord}.
     */
    protected Collection<R> beforeInsert(Collection<R> records) {
        if (records == null) {
            return Collections.emptyList();
        }
        return records.stream().filter(this::beforeInsert).collect(Collectors.toList());
    }

    /**
     * To do some stuff after records was inserted in the database.
     *
     * @param records a collection of instances of any class derived from {@link UpdatableRecord}.
     */
    protected void afterInsert(Collection<R> records) {
        records.forEach(this::afterInsert);
    }

    @Override
    public Integer insert(R record) {
        if (beforeInsert(record)) {
            int r = getDsl().executeInsert(record);
            afterInsert(record);
            return r;
        }
        return 0;
    }

    @SafeVarargs
    @Override
    public final Integer insert(R... records) {
        return insert(Arrays.asList(records));
    }

    @Override
    public Integer insert(Collection<R> records) {
        final Collection<R> recordsToInsert = beforeInsert(records);
        final int sum = Arrays.stream(
                getDsl().batchInsert(recordsToInsert).execute()
        ).sum();
        afterInsert(recordsToInsert);
        return sum;
    }

    /* * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                UPDATE                                                          *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * To performs validations and some stuff before updating the record in the database.
     *
     * @param record an instance of any class derived from {@link UpdatableRecord}.
     * @return {@code true} if this record is absolutely valid for update; otherwise returns {@code false}.
     */
    protected boolean beforeUpdate(R record) {
        return record != null && record.get(getIdField()) != null;
    }

    /**
     * To do some stuff after record was updated in the database.
     *
     * @param record an instance of any class derived from {@link UpdatableRecord}.
     */
    protected void afterUpdate(R record) {

    }

    /**
     * To performs validations and some stuff before updating the records in the database.
     *
     * @param records a collection of instance of any class derived from {@link UpdatableRecord}.
     * @return a collection of instances of any class derived from {@link UpdatableRecord}.
     */
    protected Collection<R> beforeUpdate(Collection<R> records) {
        if (records == null) {
            return Collections.emptyList();
        }
        return records.stream().filter(this::beforeUpdate).collect(Collectors.toList());
    }

    /**
     * To do some stuff after records was updated in the database.
     *
     * @param records a collection of instance of any class derived from {@link UpdatableRecord}.
     */
    protected void afterUpdate(Collection<R> records) {
        records.forEach(this::afterUpdate);
    }

    @Override
    public Integer update(R record) {
        if (beforeUpdate(record)) {
            int r = getDsl().executeUpdate(record);
            afterUpdate(record);
            return r;
        }
        return 0;
    }

    @SafeVarargs
    @Override
    public final Integer update(R... record) {
        return update(Arrays.asList(record));
    }

    @Override
    public Integer update(Collection<R> records) {
        final Collection<R> recordsToUpdate = beforeUpdate(records);
        final int sum = Arrays.stream(
                getDsl().batchUpdate(recordsToUpdate).execute()
        ).sum();
        afterUpdate(recordsToUpdate);
        return sum;
    }

    /* * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                DELETE                                                          *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * To performs validations and some stuff before deleting the record from the database.
     *
     * @param id the identifier of the record.
     * @return {@code true} if this identifier is absolutely valid for delete; otherwise returns {@code false}.
     */
    protected boolean beforeDelete(K id) {
        return id != null;
    }

    /**
     * To do some stuff after record was deleted from the database.
     *
     * @param id the identifier of the record.
     */
    protected void afterDelete(K id) {

    }

    /**
     * To performs validations and some stuff before deleting the records from the database.
     *
     * @param ids the identifier collection.
     * @return a collection of identifiers.
     */
    protected Collection<K> beforeDelete(Collection<K> ids) {
        if (ids == null) {
            return Collections.emptyList();
        }
        return ids.stream().filter(this::beforeDelete).collect(Collectors.toList());
    }

    /**
     * To do some stuff after records was updated in the database.
     *
     * @param ids the identifier collection.
     */
    protected void afterDelete(Collection<K> ids) {
        ids.forEach(this::afterDelete);
    }

    @Override
    public Integer delete(K id) {
        if (beforeDelete(id)) {
            final int execute = getDsl().deleteFrom(getTable()).where(getIdField().eq(id)).execute();
            afterDelete(id);
            return execute;
        }
        return 0;
    }

    @SafeVarargs
    @Override
    public final Integer delete(K... ids) {
        return delete(Arrays.asList(ids));
    }

    @Override
    public Integer delete(Collection<K> ids) {
        final Collection<K> idsToDelete = beforeDelete(ids);
        if (idsToDelete.isEmpty()) {
            return 0;
        }
        final int deleted = getDsl().deleteFrom(getTable()).where(getIdField().in(idsToDelete)).execute();
        afterDelete(idsToDelete);
        return deleted;
    }
}
