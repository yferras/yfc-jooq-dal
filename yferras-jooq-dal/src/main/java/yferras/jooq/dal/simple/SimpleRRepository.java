package yferras.jooq.dal.simple;

import yferras.jooq.dal.RRepository;
import yferras.util.pagination.Page;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @param <E> Any type
 * @param <K> Any type from the key of the record.
 */
public interface SimpleRRepository<E extends Serializable, K> extends RRepository<E, K, Optional<E>, List<E>, Boolean, Page<E>> {

}
