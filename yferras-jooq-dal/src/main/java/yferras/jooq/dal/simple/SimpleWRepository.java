package yferras.jooq.dal.simple;

import yferras.jooq.dal.WRepository;

/**
 * @param <E> Any type
 * @param <K> Any type from the key of the record.
 */
public interface SimpleWRepository<E, K> extends
        WRepository<E, K, Integer, Integer, Integer, Integer, Integer, Integer> {

}
