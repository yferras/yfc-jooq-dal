package yferras.jooq.dal;

import java.util.Collection;

/**
 * Read/Write repository.
 *
 * @param <E>  Any type
 * @param <K>  Any type from the key of the record.
 * @param <M0> Any type for the result of {@link #get(Object)}
 * @param <M1> Any type for the result of {@link #getByExample(Object)}
 * @param <M2> Any type for the result of {@link #exists(Object)}
 * @param <M3> Any type for the result of {@link #getAll(yferras.util.pagination.PageRequest)}
 * @param <M4> Any type for the result of {@link #insert(Object)}.
 * @param <M5> Any type for the result of {@link #insert(Object[])} or {@link #insert(Collection)}.
 * @param <M6> Any type for the result of {@link #update(Object)}.
 * @param <M7> Any type for the result of {@link #update(Object[])} or {@link #update(Collection)}.
 * @param <M8> Any type for the result of {@link #delete(Object)}.
 * @param <M9> Any type for the result of {@link #delete(Object[])} or {@link #delete(Collection)}.
 */
public interface RWRepository<E, K, M0, M1, M2, M3, M4, M5, M6, M7, M8, M9> extends
        RRepository<E, K, M0, M1, M2, M3>,
        WRepository<E, K, M4, M5, M6, M7, M8, M9> {

}
