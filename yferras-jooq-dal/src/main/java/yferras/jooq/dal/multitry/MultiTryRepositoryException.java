package yferras.jooq.dal.multitry;

import lombok.Getter;
import yferras.jooq.dal.common.Action;

import java.util.Collection;

@Getter
public class MultiTryRepositoryException extends RuntimeException {

    private final Collection<Exception> exceptions;
    private final Action action;

    public MultiTryRepositoryException(Collection<Exception> exceptions, Action action) {
        this.exceptions = exceptions;
        this.action = action;
    }

}
