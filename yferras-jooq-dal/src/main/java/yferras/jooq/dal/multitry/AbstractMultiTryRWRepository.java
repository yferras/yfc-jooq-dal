package yferras.jooq.dal.multitry;

import lombok.extern.slf4j.Slf4j;
import org.jooq.UpdatableRecord;
import yferras.jooq.dal.common.Action;
import yferras.jooq.dal.simple.AbstractSimpleRWRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;


@Slf4j
public abstract class AbstractMultiTryRWRepository<R extends UpdatableRecord<?>, K>
        extends AbstractSimpleRWRepository<R, K> {

    protected interface OnErrorConsumer<T> {
        void accept(int tryNumber, T input, Exception exception);
    }

    /**
     * Gets the max tries. By default returns 3.
     *
     * @return the max tries number.
     */
    protected int getMaxTries() {
        return 3;
    }

    protected <A, B> B executeAction(Function<A, B> function, A input, Action action, OnErrorConsumer<A> onError) {
        List<Exception> errors = new ArrayList<>(getMaxTries());
        int i = 1;
        while (i <= getMaxTries()) {
            try {
                return function.apply(input);
            } catch (Exception e) {
                errors.add(e);
                onError.accept(i, input, e);
            }
            i++;
        }
        throw new MultiTryRepositoryException(errors, action);
    }

    /* * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                INSERT                                                          *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    @Override
    public Integer insert(R record) {
        return executeAction(super::insert, record, Action.INSERT, this::onInsertError);
    }

    protected abstract void onInsertError(int tryNumber, R record, Exception exception);

    @Override
    public Integer insert(Collection<R> records) {
        return executeAction(super::insert, records, Action.BATCH_INSERT, this::onInsertError);
    }

    protected abstract void onInsertError(int tryNumber, Collection<R> records, Exception exception);

    /* * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                UPDATE                                                          *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    @Override
    public Integer update(R record) {
        return executeAction(super::update, record, Action.UPDATE, this::onUpdateError);
    }

    protected abstract void onUpdateError(int tryNumber, R record, Exception exception);

    @Override
    public Integer update(Collection<R> records) {
        return executeAction(super::update, records, Action.BATCH_UPDATE, this::onUpdateError);
    }

    protected abstract void onUpdateError(int tryNumber, Collection<R> record, Exception exception);

    /* * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                DELETE                                                          *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    @Override
    public Integer delete(K id) {
        return executeAction(super::delete, id, Action.DELETE, this::onDeleteError);
    }

    protected abstract void onDeleteError(int tryNumber, K id, Exception exception);

    @Override
    public Integer delete(Collection<K> ids) {
        return executeAction(super::delete, ids, Action.BATCH_DELETE, this::onDeleteError);
    }

    protected abstract void onDeleteError(int tryNumber, Collection<K> ids, Exception exception);
}
