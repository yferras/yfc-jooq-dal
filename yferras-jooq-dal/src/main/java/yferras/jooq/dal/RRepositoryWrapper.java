package yferras.jooq.dal;

/**
 * @param <E>  Any type
 * @param <K>  Any type from the key of the record.
 * @param <R> Any class derived from {@link RRepository}
 */
public interface RRepositoryWrapper<E, K, R extends RRepository<E, K, ?, ?, ?, ?>> {

    /**
     * Gets the wrapped repository.
     *
     * @return an instance of any type derived from: {@link RRepository}
     */
    R getWrappedRRepository();

}
