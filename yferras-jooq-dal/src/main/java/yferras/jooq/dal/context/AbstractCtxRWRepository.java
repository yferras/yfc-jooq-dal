package yferras.jooq.dal.context;

import lombok.extern.slf4j.Slf4j;
import org.jooq.UpdatableRecord;
import yferras.jooq.dal.RWRepository;
import yferras.jooq.dal.common.Action;
import yferras.util.pagination.PageRequest;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;


@Slf4j
public abstract class AbstractCtxRWRepository<E extends UpdatableRecord<?>, K, R extends RWRepository<E, K, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?>>
        implements CtxRWRepository<E, K, R> {

    private final R repository;

    protected AbstractCtxRWRepository(R repository) {
        this.repository = repository;
    }

    @Override
    public R getWrappedRepository() {
        return repository;
    }

    private <A> Context executeAction(Function<A, ?> function, A input, Action action) {
        Objects.requireNonNull(input, "'input' is null.");
        final Instant startedAt = Instant.now();
        Object result = null;
        Exception exception = null;
        try {
            result = function.apply(input);
        } catch (Exception e) {
            exception = e;
        }
        final Instant endedAt = Instant.now();
        return Context.builder()
                .action(action)
                .startedAt(startedAt)
                .endedAt(endedAt)
                .argument(input)
                .result(result)
                .exception(exception)
                .build();
    }

    private <A> Context executeAction(Function<Collection<A>, ?> function, Collection<A> collection, Action action) {
        Objects.requireNonNull(collection, "'collection' is null.");
        final Instant startedAt = Instant.now();
        Object result = null;
        Exception exception = null;
        try {
            result = function.apply(collection);
        } catch (Exception e) {
            exception = e;
        }
        final Instant endedAt = Instant.now();
        return Context.builder()
                .action(action)
                .startedAt(startedAt)
                .endedAt(endedAt)
                .argument(collection)
                .result(result)
                .exception(exception)
                .build();
    }

    @Override
    public Context get(K id) {
        return executeAction(k -> getWrappedRepository().get(k), id, Action.SELECT_BY_ID);
    }

    @Override
    public Context getByExample(E example) {
        return executeAction(e -> getWrappedRepository().getByExample(e), example, Action.SELECT_BY_EXAMPLE);
    }

    @Override
    public Context exists(E example) {
        return executeAction(e -> getWrappedRepository().exists(e), example, Action.SELECT_BY_EXAMPLE);
    }

    @Override
    public Context getAll(PageRequest pageRequest) {
        return executeAction(request -> getWrappedRepository().getAll(request), pageRequest, Action.SELECT);
    }

    @Override
    public Context insert(E record) {
        return executeAction(entity -> getWrappedRepository().insert(entity), record, Action.INSERT);
    }

    @SafeVarargs
    @Override
    public final Context insert(E... records) {
        return insert(Arrays.asList(records));
    }

    @Override
    public Context insert(Collection<E> records) {
        return executeAction(entities -> getWrappedRepository().insert(entities), records, Action.BATCH_INSERT);
    }

    @Override
    public Context update(E record) {
        return executeAction(entity -> getWrappedRepository().update(entity), record, Action.UPDATE);
    }

    @SafeVarargs
    @Override
    public final Context update(E... record) {
        return update(Arrays.asList(record));
    }

    @Override
    public Context update(Collection<E> records) {
        return executeAction(entities -> getWrappedRepository().update(entities), records, Action.BATCH_UPDATE);
    }

    @Override
    public Context delete(K id) {
        return executeAction(k -> getWrappedRepository().delete(k), id, Action.DELETE);
    }

    @SafeVarargs
    @Override
    public final Context delete(K... ids) {
        return delete(Arrays.asList(ids));
    }

    @Override
    public Context delete(Collection<K> ids) {
        return executeAction(ks -> getWrappedRepository().delete(ks), ids, Action.BATCH_DELETE);
    }

}
