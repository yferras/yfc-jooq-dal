package yferras.jooq.dal.context;

import lombok.Builder;
import lombok.Getter;
import yferras.jooq.dal.common.Action;
import yferras.util.pagination.Page;
import yferras.util.pagination.PageRequest;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Stream;

@Getter
public final class Context {

    private final Action action;
    private final int inputSize;
    private final int outputSize;
    private final Instant startedAt;
    private final Instant endedAt;
    private final Object argument;
    private final Exception exception;
    private final Object result;
    private final Duration duration;
    private final float speed;

    @Builder
    public Context(Action action, Instant startedAt, Instant endedAt, Object argument, Exception exception, Object result) {
        Objects.requireNonNull(startedAt, "'startedAt' is null.");
        Objects.requireNonNull(endedAt, "'endedAt' is null.");
        this.action = action;
        this.startedAt = startedAt;
        this.endedAt = endedAt;
        this.argument = argument;
        this.exception = exception;
        this.result = result;
        inputSize = calcInputSize(argument);
        outputSize = calcOutputSize(result);
        duration = Duration.between(startedAt, endedAt);
        speed = calcSpeed();
    }

    private static Optional<Integer> getPageRequestSize(Object obj) {
        if (obj instanceof PageRequest) {
            return Optional.of(((PageRequest) obj).getSize());
        }
        return Optional.empty();
    }

    private static Optional<Integer> getPageSize(Object obj) {
        if (obj instanceof Page) {
            return Optional.of(((Page<?>) obj).getList().size());
        }
        return Optional.empty();
    }

    private static Optional<Integer> getCollectionSize(Object object) {
        if (object instanceof Collection<?>) {
            return Optional.of(((Collection<?>) object).size());
        }
        return Optional.empty();
    }

    private static Optional<Integer> getBooleanSize(Object object) {
        if (object instanceof Boolean) {
            final boolean isTrue = (boolean) object;
            return Optional.of(isTrue ? 1 : 0);
        }
        return Optional.empty();
    }

    private static Optional<Integer> getOptionalSize(Object object) {
        if (object instanceof Optional<?>) {
            final Optional<?> optional = (Optional<?>) object;
            return Optional.of(optional.isPresent() ? 1 : 0);
        }
        return Optional.empty();
    }

    private static Optional<Integer> getNumberSize(Object object) {
        if (object instanceof Number) {
            final Number number = (Number) object;
            return Optional.of(number.intValue());
        }
        return Optional.empty();
    }


    private int calcInputSize(Object argument) {
        if (argument == null) {
            return 0;
        }
        return Stream.of(
                getCollectionSize(argument),
                getPageRequestSize(argument)
        )
                .filter(Optional::isPresent)
                .findFirst()
                .orElseGet(Optional::empty)
                .orElse(1)
                ;
    }

    private int calcOutputSize(Object result) {
        if (result == null) {
            return 0;
        }
        return Stream.of(
                getCollectionSize(result),
                getPageSize(result),
                getBooleanSize(result),
                getOptionalSize(result),
                getNumberSize(result)
        )
                .filter(Optional::isPresent)
                .findFirst()
                .orElseGet(Optional::empty)
                .orElse(1)
                ;
    }

    private float calcSpeed() {
        if (exception != null) {
            return 0;
        }
        // * 1000 to converts from ms to s
        return (getOutputSize() / (float) getDuration().toMillis()) * 1000F;
    }

    @SuppressWarnings("unchecked")
    public <T> T getResult() {
        return (T) result;
    }

    @SuppressWarnings("unchecked")
    public <T> T getArgument() {
        return (T) argument;
    }
}
