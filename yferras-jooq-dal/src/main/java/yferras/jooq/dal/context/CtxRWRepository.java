package yferras.jooq.dal.context;

import yferras.jooq.dal.RWRepository;
import yferras.jooq.dal.RWRepositoryWrapper;

/**
 * @param <E> Any type
 * @param <K> Any type from the key of the record.
 */
public interface CtxRWRepository<E, K, R extends RWRepository<E, K, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?>> extends
        RWRepository<E, K, Context, Context, Context, Context, Context, Context, Context, Context, Context, Context>,
        RWRepositoryWrapper<E, K, R> {

}
