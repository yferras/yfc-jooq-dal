package yferras.jooq.dal;

/**
 * @param <E>   Any type
 * @param <K>   Any type from the key of the record.
 * @param <R> Any class derived from {@link RWRepository}
 */
public interface RWRepositoryWrapper<E, K, R extends RWRepository<E, K, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?>> {

    /**
     * Gets the wrapped repository.
     *
     * @return an instance of any type derived from: {@link RWRepository}
     */
    R getWrappedRepository();

}
