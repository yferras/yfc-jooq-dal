package yferras.jooq.dal;

import yferras.util.pagination.PageRequest;

/**
 * Read only repository.
 *
 * @param <E>   Any type of entity.
 * @param <K>   Any type for the key's entity.
 * @param <M0>   Any type for the result of {@link #get(Object)}
 * @param <M1> Any type for the result of {@link #getByExample(Object)}
 * @param <M2>  Any type for the result of {@link #exists(Object)}
 * @param <M3>  Any type for the result of {@link #getAll(PageRequest)}
 */
public interface RRepository<E, K, M0, M1, M2, M3> {
    /**
     * Gets the "entity" from its identifier.
     *
     * @param key identifier.
     * @return the result.
     */
    M0 get(K key);

    /**
     * Gets the "entities" that matches with the example.
     *
     * @param example example entity.
     * @return the result.
     */
    M1 getByExample(E example);

    /**
     * Checks if a particular combination of data exists or not.
     *
     * @param example entity example.
     * @return the result.
     */
    M2 exists(E example);

    /**
     * Gets an instance of {@link yferras.util.pagination.DefaultPage} wrapping info, list of result included.
     *
     * @param pageRequest instance of {@link PageRequest}
     * @return the result.
     */
    M3 getAll(PageRequest pageRequest);
}
