package yferras.jooq.dal;

import jooq.autogenerated.tables.records.CompanyRecord;
import org.jooq.exception.DataAccessException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import yferras.util.pagination.DefaultPageRequest;
import yferras.util.pagination.Page;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static yferras.jooq.dal.TestUtil.getCompanyRecord;

class DefaultSimpleRWRepositoryTest {

    private final DefaultSimpleRWRepository updatableRepository = new DefaultSimpleRWRepository();

    DefaultSimpleRWRepositoryTest() throws SQLException {
    }


    @Test
    @DisplayName("Inserts one record.")
    void insert() {
        CompanyRecord record = getCompanyRecord("Company_A", 1, 2);
        assertEquals(1, updatableRepository.insert(record));
        check(record, 1);
    }

    private void check(CompanyRecord ref, int id) {
        final Optional<CompanyRecord> optional = updatableRepository.get(id);
        assertTrue(optional.isPresent());
        final CompanyRecord recordFromDb = optional.get();
        assertEquals(id, recordFromDb.getId());
        assertEquals(recordFromDb.getName(), ref.getName());
        assertEquals(recordFromDb.getGeoLat(), ref.getGeoLat());
        assertEquals(recordFromDb.getGeoLon(), ref.getGeoLon());
    }

    @Test
    @DisplayName("Inserts two records with sam data.")
    void insertThrowsError() {
        // This is OK
        assertEquals(1, updatableRepository.insert(getCompanyRecord("Company_A", 0, 0)));
        final CompanyRecord companyA = getCompanyRecord("Company_A", 0, 0);
        assertThrows(DataAccessException.class,
                // Now an exception will be thrown
                () -> updatableRepository.insert(companyA)
        );
    }

    @Test
    @DisplayName("Validations.")
    void validations() {
        assertEquals(0, updatableRepository.insert((CompanyRecord) null));
        assertEquals(0, updatableRepository.insert((Collection<CompanyRecord>) null));
        assertEquals(0, updatableRepository.update((CompanyRecord) null));
        assertEquals(0, updatableRepository.update((Collection<CompanyRecord>) null));
        assertEquals(0, updatableRepository.delete((Integer) null));
        assertEquals(0, updatableRepository.delete((Collection<Integer>) null));
    }

    @Test
    @DisplayName("Inserts 3 records as array.")
    void testInsert() {
        CompanyRecord record1 = getCompanyRecord("Company_A", 0, 0);
        CompanyRecord record2 = getCompanyRecord("Company_B", 10, 10);
        CompanyRecord record3 = getCompanyRecord("Company_C", -10, -10);

        assertEquals(3, updatableRepository.insert(record1, record2, record3));
        check(record1, 1);
        check(record2, 2);
        check(record3, 3);
    }

    @Test
    @DisplayName("Inserts a collection of 5 records.")
    void testInsert1() {
        List<CompanyRecord> companyRecords = new ArrayList<>();
        companyRecords.add(getCompanyRecord("Company_A", 0, 0));
        companyRecords.add(getCompanyRecord("Company_B", 10, 10));
        companyRecords.add(getCompanyRecord("Company_C", -10, -10));
        companyRecords.add(getCompanyRecord("Company_D", 20, -10));
        companyRecords.add(getCompanyRecord("Company_E", 20, -20));

        assertEquals(5, updatableRepository.insert(companyRecords));
        for (int id = 1; id <= companyRecords.size(); id++) {
            check(companyRecords.get(id - 1), id);
        }
    }

    @Test
    @DisplayName("Updates database only one record.")
    void update() {
        CompanyRecord record = getCompanyRecord("Company_A", 0, 0);
        assertEquals(1, updatableRepository.insert(record));
        // Inserted,

        // then modified. And try to update.
        // The record will be not updated, it no has an identifier.
        record.setGeoLat(10);
        record.setGeoLon(10);
        assertEquals(0, updatableRepository.update(record));

        // get the record from the database, then modify it and finally update database.
        final Optional<CompanyRecord> optional = updatableRepository.get(1);
        if (!optional.isPresent()) {
            fail("In database the id 1 must be exist");
        }
        record = optional.get();
        record.setGeoLat(10);
        record.setGeoLon(10);
        assertEquals(1, updatableRepository.update(record));
        check(record, 1);
    }

    @Test
    @DisplayName("Updates database using an array or a collection of records.")
    void testUpdate() {
        List<CompanyRecord> companyRecords = new ArrayList<>();
        companyRecords.add(getCompanyRecord("Company_A", 0, 0));
        companyRecords.add(getCompanyRecord("Company_B", 10, 10));
        companyRecords.add(getCompanyRecord("Company_C", -10, -10));
        companyRecords.add(getCompanyRecord("Company_D", 20, -10));
        companyRecords.add(getCompanyRecord("Company_E", 20, -20));
        assertEquals(5, updatableRepository.insert(companyRecords));
        final Optional<CompanyRecord> optionalId2 = updatableRepository.get(2);
        if (!optionalId2.isPresent()) {
            fail("In database the id 2 must be exist");
        }
        final Optional<CompanyRecord> optionalId4 = updatableRepository.get(4);
        if (!optionalId4.isPresent()) {
            fail("In database the id 4 must be exist");
        }

        final CompanyRecord companyRecordId2 = optionalId2.get();
        final CompanyRecord companyRecordId4 = optionalId4.get();

        // Update as array.
        companyRecordId2.setName("AAA");
        companyRecordId4.setName("ZZZ");
        assertEquals(2, updatableRepository.update(companyRecordId2, companyRecordId4));
        check(companyRecordId2, 2);
        check(companyRecordId4, 4);

        // Update as collection.
        companyRecordId2.setName("+++");
        companyRecordId2.setGeoLat(500);
        companyRecordId2.setGeoLon(500);
        companyRecordId4.setName("---");
        companyRecordId4.setGeoLat(-500);
        companyRecordId4.setGeoLon(-500);
        List<CompanyRecord> listToUpdate = new ArrayList<>();
        listToUpdate.add(companyRecordId2);
        listToUpdate.add(companyRecordId4);
        assertEquals(2, updatableRepository.update(listToUpdate));
        check(companyRecordId2, 2);
        check(companyRecordId4, 4);
    }


    @Test
    @DisplayName("Deletes from database using only one identifier.")
    void delete() {
        List<CompanyRecord> companyRecords = new ArrayList<>();
        companyRecords.add(getCompanyRecord("Company_A", 0, 0));
        companyRecords.add(getCompanyRecord("Company_B", 10, 10));
        companyRecords.add(getCompanyRecord("Company_C", -10, -10));
        companyRecords.add(getCompanyRecord("Company_D", 20, -10));
        companyRecords.add(getCompanyRecord("Company_E", 20, -20));
        assertEquals(5, updatableRepository.insert(companyRecords));

        for (int id = 1; id <= companyRecords.size(); id++) {
            assertEquals(1, updatableRepository.delete(id));
        }
        final Page<CompanyRecord> page = updatableRepository.getAll(DefaultPageRequest.of(1, 10));
        assertTrue(page.getList().isEmpty());
    }

    @Test
    @DisplayName("Deletes from database using an array and a collection of identifiers.")
    void testDelete() {
        List<CompanyRecord> companyRecords = new ArrayList<>();
        companyRecords.add(getCompanyRecord("Company_A", 0, 0));
        companyRecords.add(getCompanyRecord("Company_B", 10, 10));
        companyRecords.add(getCompanyRecord("Company_C", -10, -10));
        companyRecords.add(getCompanyRecord("Company_D", 20, -10));
        companyRecords.add(getCompanyRecord("Company_E", 20, -20));
        assertEquals(5, updatableRepository.insert(companyRecords));

        assertEquals(2, updatableRepository.delete(1, 2));
        List<Integer> ids = new ArrayList<>();
        ids.add(3);
        ids.add(4);
        ids.add(5);
        assertEquals(3, updatableRepository.delete(ids));
        final Page<CompanyRecord> page = updatableRepository.getAll(DefaultPageRequest.of(1, 10));
        assertTrue(page.getList().isEmpty());
    }

}