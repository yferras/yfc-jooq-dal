package yferras.jooq.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2 {
    public static Connection getConnection() throws SQLException {
        String userName = "sa";
        String password = "";
        String url = "jdbc:h2:mem:learnalang;MODE=PostgreSQL;DB_CLOSE_DELAY=-1;SCHEMA=PUBLIC;DATABASE_TO_LOWER=TRUE;INIT=RUNSCRIPT FROM '../scripts/create.sql'";
        return DriverManager.getConnection(url, userName, password);
    }
}
