package yferras.jooq.dal.context;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import yferras.util.pagination.DefaultPage;
import yferras.util.pagination.DefaultPageRequest;
import yferras.util.pagination.Page;
import yferras.util.pagination.PageRequest;

import java.io.Serializable;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ContextTest {

    public static final List<Serializable> OBJ_COLLECTION = Arrays.asList(
            1,
            2,
            3,
            4,
            5
    );
    public static final PageRequest PAGE_REQUEST = DefaultPageRequest.of(1, 100);
    public static final Page<Serializable> PAGE = DefaultPage.of(OBJ_COLLECTION, PAGE_REQUEST, 5);

    @Test
    void testContextActionInputSize0() {
        final Context context = Context.builder()
                .argument(null) // If argument == null the inputSize must be 0.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertNull(context.getArgument());
        assertEquals(0, context.getInputSize());
    }

    @Test
    void testContextActionInputSizeN() {
        Context context = Context.builder()
                .argument(new Object()) // If argument is an object the inputSize must be 1.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertNotNull(context.getArgument());
        assertEquals(1, context.getInputSize());

        context = Context.builder()
                .argument(
                        OBJ_COLLECTION
                ) // If argument is a collection the inputSize must be the size of the collection.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(5, context.getInputSize());

        context = Context.builder()
                .argument(PAGE_REQUEST) // If argument is a PageRequest the inputSize must be the size of the page.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(100, context.getInputSize());
    }

    @Test
    void testContextActionOutputSize0() {
        Context context = Context.builder()
                .result(null) // If result is an object the outputSize must be 1.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertNull(context.getResult());
        assertEquals(0, context.getInputSize());
    }

    @Test
    void testContextActionOutputSizeN() {
        Context context = Context.builder()
                .result(new Object()) // If result is an object, the outputSize must be 1.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertNotNull(context.getResult());
        assertEquals(1, context.getOutputSize());

        context = Context.builder()
                .result(1_000) // If result is a number, the outputSize must be the in part of the number.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(1_000, context.getOutputSize());

        context = Context.builder()
                .result(true) // If result is true, the outputSize must be 1.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(1, context.getOutputSize());

        context = Context.builder()
                .result(false) // If result is false, the outputSize must be 0.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(0, context.getOutputSize());

        context = Context.builder()
                .result(Optional.of(new Object())) // If result is mot empty, the outputSize must be 1.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(1, context.getOutputSize());

        context = Context.builder()
                .result(Optional.empty()) // If result is empty, the outputSize must be 0.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(0, context.getOutputSize());

        context = Context.builder()
                .result(
                        OBJ_COLLECTION
                ) // If result is a collection, the outputSize must be the size of the collection.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(5, context.getOutputSize());

        context = Context.builder()
                .result(
                        PAGE
                ) // If result is a Page, the outputSize must be the size of the page.
                .startedAt(Instant.now())
                .endedAt(Instant.now())
                .build();
        assertEquals(5, context.getOutputSize());
    }

    @Test
    void testContextDurationSped() throws InterruptedException {
        final long time = 1_000L;
        final Instant startedAt = Instant.now();
        TimeUnit.MILLISECONDS.sleep(time);
        final Instant endedAt = Instant.now();

        Context context = Context.builder()
                .result(1_000)
                .startedAt(startedAt)
                .endedAt(endedAt)
                .build();

        assertNotNull(context.getStartedAt());
        assertNotNull(context.getEndedAt());
        assertTrue(context.getDuration().toMillis() >= time);
        assertEquals(1_000F, context.getSpeed());

    }

}