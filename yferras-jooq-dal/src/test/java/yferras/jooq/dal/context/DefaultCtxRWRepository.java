package yferras.jooq.dal.context;

import jooq.autogenerated.tables.records.CompanyRecord;
import yferras.jooq.dal.DefaultSimpleRWRepository;

import java.sql.SQLException;

class DefaultCtxRWRepository extends AbstractCtxRWRepository<CompanyRecord, Integer, DefaultSimpleRWRepository> {

    public DefaultCtxRWRepository() throws SQLException {
        super(new DefaultSimpleRWRepository());
    }
}
