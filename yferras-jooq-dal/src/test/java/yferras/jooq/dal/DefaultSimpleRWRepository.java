package yferras.jooq.dal;

import jooq.autogenerated.tables.records.CompanyRecord;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.impl.DSL;
import yferras.jooq.dal.simple.AbstractSimpleRWRepository;

import java.sql.Connection;
import java.sql.SQLException;

import static jooq.autogenerated.tables.Company.COMPANY;

public class DefaultSimpleRWRepository extends AbstractSimpleRWRepository<CompanyRecord, Integer> {

    private final DSLContext dsl;

    public DefaultSimpleRWRepository() throws SQLException {
        final Connection connection = H2.getConnection();
        dsl = DSL.using(connection);
    }

    @Override
    protected DSLContext getDsl() {
        return dsl;
    }

    @Override
    protected Field<Integer> getIdField() {
        return COMPANY.ID;
    }

    @Override
    protected Table<CompanyRecord> getTable() {
        return COMPANY;
    }
}
